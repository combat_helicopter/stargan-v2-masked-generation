import os

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


SUFFIXES = {"png", "jpg", "jpeg", "JPEG", "JPG"}
PREFFIXES = {"DJI"}

def image_coroutine(dir_link, default_name, image_type):
    image_links = sorted([l for l in os.listdir(dir_link) if l[:3] in PREFFIXES])
    for i, il in enumerate(image_links):
        image_path = os.path.join(dir_link, il, default_name)

        if image_type == 'mask':
            img = Image.open(image_path)
        else:
            img = Image.open(image_path).convert("RGB")
        img = np.asarray(img)

        yield img


def crop_coroutine(img, size=256):
    rows, cols = img.shape[:2]
    rows, cols = rows // 256, cols // 256

    for row in range(rows):
        for col in range(cols):
            crop = img[size * row : size * (row + 1), size * col : size * (col + 1)]

            yield crop


def one_dataset_coroutine(dir_link, default_names, image_types, n=np.inf):
    number = 0
    cors = [
        image_coroutine(dir_link, default_name=dn, image_type=it) 
        for dn, it in zip(default_names, image_types)
    ]

    try:
        while True:
            imgs = [next(cor) for cor in cors]
            crop_cors = [crop_coroutine(img) for img in imgs]

            try:
                while True:
                    crops = [next(crop_cor) for crop_cor in crop_cors]
                    number += 1
                    yield crops
                    if number == n:
                        return number
            except StopIteration:
                continue
    except StopIteration:
        pass

    return number


def dataset_coroutine(links_list, default_names, image_types, each_n=None, total_n=np.inf):
    number = 0
    ns_list = []
    if isinstance(each_n, (int, float)):
        ns_list = [each_n] * len(links_list)
    if isinstance(each_n, list):
        assert len(each_n) == len(links_list)
        ns_list = each_n

    for ni, link in zip(ns_list, links_list):
        ni = min(ni, total_n)
        print(ni)
        odc = one_dataset_coroutine(link, default_names, image_types, ni)
        odc_number = yield from odc

        print('odc_number: ', odc_number)

        number += odc_number
        total_n -= odc_number
        if total_n == 0:
            break

    print('total_number: ', number)
    return number