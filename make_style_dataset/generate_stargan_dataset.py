from time import time
import os
import shutil
from PIL import Image

from assign_labels import assign_labels
from build_image_hist import get_color_histograms_of_images, color_hist_to_2d


def make_stargandataset_dir(datasets_path, name, del_prev=False):
    stargan_dataset_path = os.path.join(datasets_path, name)
    os.makedirs(stargan_dataset_path, mode=0o777, exist_ok=True)
    for i in range(1, 12):
        name = 'class_00'
        name_number = str(i)
        name = name[:-len(name_number)] + name_number

        class_path = os.path.join(stargan_dataset_path, name)
        if del_prev & os.path.exists(class_path):
            shutil.rmtree(class_path)
        os.makedirs(class_path, mode=0o777, exist_ok=True)


def save_batch(batch, numbers, labels, dataset_path, name_pattern='000000'):
    for img, numb, lab in zip(batch, numbers, labels):
        numb = f'{numb}'
        name = name_pattern[:-len(numb)] + numb
        
        img_path = os.path.join(dataset_path, f'class_{lab}', f'{name}.jpeg')
        Image.fromarray(img).save(img_path)


def save_batch(batch, numbers, labels, dataset_path, 
               name_pattern='000000', class_pattern='class_00'):
    for img, numb, lab in zip(batch, numbers, labels):
        numb = f'{numb}'
        name = name_pattern[:-len(numb)] + numb
        
        class_name = str(lab)
        class_name = class_pattern[:-len(class_name)] + class_name
        img_path = os.path.join(dataset_path, class_name, f'{name}.jpeg')
        Image.fromarray(img).save(img_path)


def generate_stargan_dataset(reducer, dataset_coroutine, batch_size, stargan_dataset_path):
    batch = []
    img_numbers = []
    current_img = 1
    while True:
        t1 = time()
        for i in range(batch_size):
            img = next(dataset_coroutine)
            batch.append(img)
            img_numbers.append(current_img)
            current_img += 1

        batch_hists = get_color_histograms_of_images(batch)
        batch_umap_hists = color_hist_to_2d(batch_hists, reducer)
        labels = assign_labels(batch_umap_hists)
        save_batch(batch, img_numbers, labels, stargan_dataset_path)
        t2 = time()
        print(f'{current_img} images was saved: {round(t2 - t1)}s')

        batch = []
        img_numbers = []