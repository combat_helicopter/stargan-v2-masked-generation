"""
StarGAN v2
Copyright (c) 2020-present NAVER Corp.

This work is licensed under the Creative Commons Attribution-NonCommercial
4.0 International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
"""

import os
from os.path import join as ospj
import time
import datetime
from munch import Munch

import torch
import torch.nn as nn
import torch.nn.functional as F

from stargan_v2.core.custom_model import build_model
from stargan_v2.core.checkpoint import CheckpointIO
from stargan_v2.core.custom_data_loader import InputFetcher
import stargan_v2.core.custom_utils as utils

from PIL import Image
import numpy as np
import wandb


def batch_to_applique(masks, real, fake, rec):
    masks = masks.detach().cpu().permute(0, 2, 3, 1)[None].repeat(1, 1, 1, 1, 3) # 1 x 8 x 256 x 256 x 3 
    real = real.detach().cpu().permute(0, 2, 3, 1)[None] # 1 x 8 x 256 x 256 x 3
    fake = fake.detach().cpu().permute(0, 2, 3, 1)[None] # 1 x 8 x 256 x 256 x 3
    rec  =  rec.detach().cpu().permute(0, 2, 3, 1)[None] # 1 x 8 x 256 x 256 x 3
    b = torch.cat([masks, real, fake, rec], dim=0)
    b[0] = b[0] * 255
    b[1:] = (b[1:] + 1) / 2 * 255
    
    b = torch.cat([
        torch.cat(tuple(s), dim=0).permute(1, 0, 2)
        for s in b
    ])
    b = np.clip(b.numpy().astype(np.uint8), 0, 255)

    b = Image.fromarray(b, mode='RGB')
    return b


class Solver(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.args = args
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.nets, self.nets_ema = build_model(args)
        # below setattrs are to make networks be children of Solver, e.g., for self.to(self.device)
        for name, module in self.nets.items():
            utils.print_network(module, name)
            setattr(self, name, module)
        for name, module in self.nets_ema.items():
            setattr(self, name + '_ema', module)

        if args.mode == 'train':
            self.optims = Munch()
            for net in self.nets.keys():
                if net == 'fan':
                    continue
                self.optims[net] = torch.optim.Adam(
                    params=self.nets[net].parameters(),
                    lr=args.f_lr if net == 'mapping_network' else args.lr,
                    betas=[args.beta1, args.beta2],
                    weight_decay=args.weight_decay)

            self.ckptios = [
                CheckpointIO(ospj(args.checkpoint_dir, '{:06d}_nets.ckpt'), data_parallel=True, **self.nets),
                CheckpointIO(ospj(args.checkpoint_dir, '{:06d}_nets_ema.ckpt'), data_parallel=True, **self.nets_ema),
                CheckpointIO(ospj(args.checkpoint_dir, '{:06d}_optims.ckpt'), **self.optims)]
        else:
            self.ckptios = [CheckpointIO(ospj(args.checkpoint_dir, '{:06d}_nets_ema.ckpt'), data_parallel=True, **self.nets_ema)]

        self.to(self.device)
        for name, network in self.named_children():
            # Do not initialize the FAN parameters
            if ('ema' not in name) and ('fan' not in name):
                print('Initializing %s...' % name)
                network.apply(utils.he_init)

    def _save_checkpoint(self, step):
        for ckptio in self.ckptios:
            ckptio.save(step)

    def _load_checkpoint(self, step):
        for ckptio in self.ckptios:
            ckptio.load(step)

    def _reset_grad(self):
        for optim in self.optims.values():
            optim.zero_grad()

    def train(self, loaders):
        args = self.args
        nets = self.nets
        nets_ema = self.nets_ema
        optims = self.optims

        # fetch random validation images for debugging
        fetcher = InputFetcher(loader_style=loaders.src_stl,
                               loader_gim=loaders.src_gim,
                               loader_ref_style_style=loaders.ref,
                               latent_dim=args.latent_dim,
                               mode='train')
        fetcher_val = InputFetcher(loader_style=loaders.src_stl,
                                   loader_gim=loaders.src_gim,
                                   loader_ref_style_style=None,
                                   latent_dim=args.latent_dim,
                                   mode='val')
        inputs_val = next(fetcher_val)

        # resume training if necessary
        if args.resume_iter > 0:
            self._load_checkpoint(args.resume_iter)

        # remember the initial value of ds weight
        initial_lambda_ds = args.lambda_ds

        print('Start training...')
        start_time = time.time()
        for i in range(args.resume_iter, args.total_iters):
            # fetch images and labels
            inputs = next(fetcher)
            x_real_style, y_real_style = inputs.x_stl, inputs.y_stl
            x_real_gim, y_real_gim = inputs.x_gim, inputs.y_gim
            x_ref, x_ref2, y_ref = inputs.x_ref, inputs.x_ref2, inputs.y_ref
            z_trg, z_trg2 = inputs.z_trg, inputs.z_trg2
            
            masks = nets.fan.get_heatmap(x_real) if args.w_hpf > 0 else None

            ###########################
            # train the discriminator #
            ###########################
            d_loss, d_losses_latent = compute_d_loss(
                nets, args, 
                x_real_style, y_real_style, # <------------------| img with empty mask, 175k examples
                y_ref, z_trg=z_trg, # <--------------------------| style from random
                masks=masks)
            self._reset_grad()
            d_loss.backward()
            optims.discriminator.step()

            d_loss, d_losses_ref = compute_d_loss(
                nets, args, 
                x_real_style, y_real_style, # <------------------| img with empty mask, 175k examples
                y_ref, x_ref=x_ref, # <--------------------------| style from reference
                masks=masks)
            self._reset_grad()
            d_loss.backward()
            optims.discriminator.step()
            
            d_loss, d_losses_latent = compute_d_loss(
                nets, args, 
                x_real_gim, y_real_gim, # <------------------| img with mask, 39k examples
                y_ref, z_trg=z_trg, # <--------------------------| style from random
                masks=masks)
            self._reset_grad()
            d_loss.backward()
            optims.discriminator.step()

            d_loss, d_losses_ref = compute_d_loss(
                nets, args, 
                x_real_gim, y_real_gim, # <------------------| img with mask, 39k examples
                y_ref, x_ref=x_ref, # <--------------------------| style from reference
                masks=masks)
            self._reset_grad()
            d_loss.backward()
            optims.discriminator.step()

            #######################
            # train the generator #
            #######################
            g_loss, g_losses_latent, _, _ = compute_g_loss(
                nets, args, 
                x_real_style, y_real_style, # <------------------| img with empty mask, 175k examples
                y_ref, z_trgs=[z_trg, z_trg2], # <---------------| style from random
                masks=masks)
            self._reset_grad()
            g_loss.backward()
            optims.generator.step()
            optims.mapping_network.step()
            optims.style_encoder.step()

            g_loss, g_losses_ref, _, _ = compute_g_loss(
                nets, args, 
                x_real_style, y_real_style, # <------------------| img with empty mask, 175k examples
                y_ref, x_refs=[x_ref, x_ref2], # <---------------| style from reference
                masks=masks)
            self._reset_grad()
            g_loss.backward()
            optims.generator.step()
            
            g_loss, g_losses_latent, x_fake_from_latent, x_rec_from_latent = compute_g_loss(
                nets, args, 
                x_real_gim, y_real_gim, # <------------------| img with mask, 39k examples
                y_ref, z_trgs=[z_trg, z_trg2], # <---------------| style from random
                masks=masks)
            self._reset_grad()
            g_loss.backward()
            optims.generator.step()
            optims.mapping_network.step()
            optims.style_encoder.step()

            g_loss, g_losses_ref, x_fake_from_real, x_rec_from_real = compute_g_loss(
                nets, args,
                x_real_gim, y_real_gim, # <------------------| img with mask, 39k examples
                y_ref, x_refs=[x_ref, x_ref2], # <---------------| style from reference
                masks=masks)
            self._reset_grad()
            g_loss.backward()
            optims.generator.step()

            # compute moving average of network parameters
            moving_average(nets.generator, nets_ema.generator, beta=0.999)
            moving_average(nets.mapping_network, nets_ema.mapping_network, beta=0.999)
            moving_average(nets.style_encoder, nets_ema.style_encoder, beta=0.999)

            # decay weight for diversity sensitive loss
            if args.lambda_ds > 0:
                args.lambda_ds -= (initial_lambda_ds / args.ds_iter)

            # print out log info
            if (i+1) % args.print_every == 0:
                elapsed = time.time() - start_time
                elapsed = str(datetime.timedelta(seconds=elapsed))[:-7]
                log = "Elapsed time [%s], Iteration [%i/%i], " % (elapsed, i+1, args.total_iters)
                all_losses = dict()
                for loss, prefix in zip([d_losses_latent, d_losses_ref, g_losses_latent, g_losses_ref],
                                        ['D/latent_', 'D/ref_', 'G/latent_', 'G/ref_']):
                    for key, value in loss.items():
                        all_losses[prefix + key] = value
                all_losses['G/lambda_ds'] = args.lambda_ds
                log += ' '.join(['%s: [%.4f]' % (key, value) for key, value in all_losses.items()])
                print(log)
                
            # generate images for debugging
            if (i+1) % args.sample_every == 0:
                os.makedirs(args.sample_dir, exist_ok=True)
                utils.debug_image(nets_ema, args, inputs=inputs_val, step=i+1)

            # save model checkpoints
            if (i+1) % args.save_every == 0:
                self._save_checkpoint(step=i+1)
            
            wandb.log({
                'g_loss': g_loss.item(), 
                'd_loss': d_loss.item()
            })
            
            if i % args.log_imgs == 0:    
                check_latent = batch_to_applique(x_real_gim[:, 3:], x_real_gim[:, :3], x_fake_from_latent, x_rec_from_latent)
                check_real = batch_to_applique(x_real_gim[:, 3:], x_real_gim[:, :3], x_fake_from_real, x_rec_from_real)
                wandb.log({
                    'real__fake_from_latent__rec': wandb.Image(check_latent),
                    'real__fake_from_ref__rec': wandb.Image(check_real)
                })


def compute_d_loss(nets, args, x_real, y_org, y_trg, z_trg=None, x_ref=None, masks=None):
    assert (z_trg is None) != (x_ref is None)
    # with real images
    x_real_img = x_real[:, :3]
    x_real_img.requires_grad_()
    out = nets.discriminator(x_real_img, y_org)
    loss_real = adv_loss(out, 1)
    loss_reg = r1_reg(out, x_real_img)

    # with fake images
    with torch.no_grad():
        if z_trg is not None:
            s_trg = nets.mapping_network(z_trg, y_trg)
        else:  # x_ref is not None
            s_trg = nets.style_encoder(x_ref, y_trg)

        x_fake = nets.generator(x_real, s_trg, masks=masks)
    out = nets.discriminator(x_fake, y_trg)
    loss_fake = adv_loss(out, 0)

    loss = loss_real + loss_fake + args.lambda_reg * loss_reg
    return loss, Munch(real=loss_real.item(),
                       fake=loss_fake.item(),
                       reg=loss_reg.item())


def compute_g_loss(nets, args, x_real, y_org, y_trg, z_trgs=None, x_refs=None, masks=None):
    assert (z_trgs is None) != (x_refs is None)

    if z_trgs is not None:
        z_trg, z_trg2 = z_trgs
    if x_refs is not None:
        x_ref, x_ref2 = x_refs

    # adversarial loss
    if z_trgs is not None:
        s_trg = nets.mapping_network(z_trg, y_trg) 
    else:
        s_trg = nets.style_encoder(x_ref, y_trg) # img + mask 

    x_fake = nets.generator(x_real, s_trg, masks=masks) # img + mask
    out = nets.discriminator(x_fake, y_trg)
    loss_adv = adv_loss(out, 1) # <-----------------------------------------------| не меняется
 
    # style reconstruction loss
    x_fake_and_mask = torch.cat([x_fake, x_real[:, 3:, :, :]], dim=1)
    s_pred = nets.style_encoder(x_fake_and_mask, y_trg) # img + mask
    loss_sty = torch.mean(torch.abs(s_pred - s_trg)) # <--------------------------| не меняется

    # diversity sensitive loss
    if z_trgs is not None:
        s_trg2 = nets.mapping_network(z_trg2, y_trg)
    else:
        s_trg2 = nets.style_encoder(x_ref2, y_trg) # img + mask

    x_fake2 = nets.generator(x_real, s_trg2, masks=masks)
    x_fake2 = x_fake2.detach()
    
    is_object_mask = (x_real[:, 3:, :, :] == 1.).repeat(1, 3, 1, 1)
    same_obj_on_fake = (x_real[:, :3] - x_fake)[is_object_mask]
    fake_ground = (x_fake - x_fake2)[~is_object_mask]
    
    loss_safe_obj_on_fake = torch.mean(torch.abs(same_obj_on_fake)) # <-----------| объект должен оставаться тем же
    loss_ds = torch.mean(torch.abs(fake_ground)) # <------------------------------| фон должен меняться

    # cycle-consistency loss
    masks = nets.fan.get_heatmap(x_fake) if args.w_hpf > 0 else None
    s_org = nets.style_encoder(x_real, y_org)
    x_rec = nets.generator(x_fake_and_mask, s_org, masks=masks)

    loss_safe_obj_on_rec = torch.mean(torch.abs(same_obj_on_fake))
    loss_cyc = torch.mean(torch.abs(x_rec - x_real[:, :3])) # <--------------------------| не меняется

    loss = loss_adv + \
           args.lambda_sty * loss_sty + \
           args.lambda_safe * loss_safe_obj_on_fake + \
           - args.lambda_ds * loss_ds + \
           args.lambda_cyc * loss_cyc

    return loss, Munch(adv=loss_adv.item(),
                       sty=loss_sty.item(),
                       safe=loss_safe_obj_on_rec.item(),
                       ds=loss_ds.item(),
                       cyc=loss_cyc.item()), x_fake, x_rec


def moving_average(model, model_test, beta=0.999):
    for param, param_test in zip(model.parameters(), model_test.parameters()):
        param_test.data = torch.lerp(param.data, param_test.data, beta)


def adv_loss(logits, target):
    assert target in [1, 0]
    targets = torch.full_like(logits, fill_value=target)
    loss = F.binary_cross_entropy_with_logits(logits, targets)
    return loss


def r1_reg(d_out, x_in):
    # zero-centered gradient penalty for real images
    batch_size = x_in.size(0)
    grad_dout = torch.autograd.grad(
        outputs=d_out.sum(), inputs=x_in,
        create_graph=True, retain_graph=True, only_inputs=True
    )[0]
    grad_dout2 = grad_dout.pow(2)
    assert(grad_dout2.size() == x_in.size())
    reg = 0.5 * grad_dout2.view(batch_size, -1).sum(1).mean(0)
    return reg