import os
import torch
import random
import numpy as np

from PIL import Image
from munch import Munch
from pathlib import Path
from itertools import chain
from torch.utils import data
from torchvision import transforms
from torchvision.datasets import ImageFolder
from torch.utils.data.sampler import WeightedRandomSampler

def listdir(dname, formats):
    fnames = list(chain(*[list(Path(dname).rglob('*.' + ext))
                          for ext in formats]))
    return fnames


##############################################################################################
###################################### STYLE DATASET #########################################
##############################################################################################
# 175.000 images for style: ImageFolder

# 175.000 images + empty mask for usage in style encoder
# mask in format (min=0. : max=0.) when style area in 0. part
class StyleDataset(data.Dataset):
    def __init__(self, root, transform=None, with_empty_mask=None, channels=0):
        self.basic_dataset = ImageFolder(root, transform=transform)
        self.targets = self.basic_dataset.targets
        self.with_empty_mask = with_empty_mask
        self.channels = channels

    def __getitem__(self, index):
        img, t = self.basic_dataset[index]

        if self.with_empty_mask:
            mask = torch.zeros(self.channels, img.shape[1], img.shape[2])
            img_and_mask = torch.cat([img, mask], dim=0)
            return img_and_mask, t

        return img, t

    def __len__(self):
        return len(self.basic_dataset)
    
    
##############################################################################################
###################################### BASIC GIM DATASET #####################################
##############################################################################################
# dataset for 39.000 generated images with masks
# 6 masks in format (min=0. : max=1.) when style area in 0. part
class BasicGIMDataset(data.Dataset):
    def __init__(self, root, formats=['npy'], transform=None):
        self.classes = sorted([
            t for t in os.listdir(root)
            if 'class' in t
        ])
        
        self.class_to_idx = {
            c: i
            for i, c in enumerate(self.classes)
        }

        self.items = [
            (path, self.class_to_idx[c])
            for c in self.classes
            for path in listdir(os.path.join(root, c), formats)
        ]

        self.targets = np.array([
            i[-1] for i in self.items
        ])

        self.transform = transform

    def __getitem__(self, index):
        pth, t = self.items[index]
        
        img_and_mask = np.load(pth).transpose(2, 0, 1)
        img_and_mask = torch.tensor(img_and_mask, dtype=torch.float32)

        if self.transform is not None:
            return self.transform(img_and_mask), t, pth

        return img_and_mask, t, pth

    def __len__(self):
        return len(self.items)
    

##############################################################################################
################################# GIM DATASET GLUED MASKS ####################################
##############################################################################################
# dataset for 39.000 generated images with masks
# 1 mask in format (min=0. : max=1.) when style area in 0. part, and garbage of 6 classes in 1.
class GIMDataset(data.Dataset):
    def __init__(self, root, formats=['npy'], th=None, transform=None):
        self.basic_dataset = BasicGIMDataset(root=root, formats=formats, transform=transform)
        self.targets = self.basic_dataset.targets
        self.th = th

    def __getitem__(self, index):
        img_and_mask, t, _ = self.basic_dataset[index]

        img = img_and_mask[:3, :, :]
        
        masks = img_and_mask[3:, :, :]
        mask = torch.max(masks, dim=0)[0][None]
        
        if self.th is not None:
            mask = (mask > self.th).to(torch.float32)
        
        img_and_mask = torch.cat([img, mask], dim=0).to(torch.float32)

        return img_and_mask, t

    def __len__(self):
        return len(self.basic_dataset)
    
    
##############################################################################################
################################ REFERENCE STYLE TRANSFORM ###################################
##############################################################################################
def one_item_loader_1(fname, transform, with_empty_mask, channels):
    img = Image.open(fname).convert('RGB')
    img = transform(img)

    if with_empty_mask:
        mask = torch.zeros(channels, img.shape[1], img.shape[2])
        img_and_mask = torch.cat([img, mask], dim=0)
        return img_and_mask
    return img

def one_item_loader_2(fname, transform, th):
    img_and_mask = np.load(fname).transpose(2, 0, 1)
    img_and_mask = torch.tensor(img_and_mask, dtype=float)

    if transform is not None:
        img_and_mask = transform(img_and_mask)
    
    img = img_and_mask[:3, :, :]
    masks = img_and_mask[3:, :, :]
    mask = torch.max(masks, dim=0)[0][None]
    
    if th is not None:
        mask = (mask > th).to(float)
        img_and_mask = torch.cat([img, mask], dim=0)

    return img_and_mask

##############################################################################################
################################ REFERENCE DATASET ###########################################
##############################################################################################
class ReferenceDataset(data.Dataset):
    def __init__(self, 
                 root_1,
                 root_2,
                 formats_1=['png', 'jpg', 'jpeg', 'JPG'],
                 formats_2=['png', 'jpg', 'jpeg', 'JPG'],
                 one_item_loader_1=None,
                 one_item_loader_2=None
                ):
        self.formats_1 = formats_1
        self.formats_2 = formats_2

        self.items, self.targets = self._make_dataset(root_1, root_2)

        self.one_item_loader_1 = one_item_loader_1
        self.one_item_loader_2 = one_item_loader_2


    def _make_dataset(self, root_1, root_2):
        classes_1 = {t for t in os.listdir(root_1) if 'class' in t}
        classes_2 = {t for t in os.listdir(root_2) if 'class' in t}
        classes = sorted(classes_1 & classes_2)

        fnames, fnames2, targets = [], [], []
        for idx, class_name in enumerate(classes):
            class_dir_1 = os.path.join(root_1, class_name)
            class_dir_2 = os.path.join(root_2, class_name)
            
            cls_fnames_1 = listdir(class_dir_1, self.formats_1)
            cls_fnames_2 = listdir(class_dir_2, self.formats_2)
            
            fnames += cls_fnames_1
            if len(cls_fnames_1) <= len(cls_fnames_2):
                fnames2 += random.sample(cls_fnames_2, len(cls_fnames_1))
            else:
                fnames2 += np.random.choice(cls_fnames_2, size=len(cls_fnames_1)).tolist()
            targets += [idx] * len(cls_fnames_1)
        return list(zip(fnames, fnames2)), targets

    def __getitem__(self, index):
        fname, fname2 = self.items[index]
        target = self.targets[index]

        img = self.one_item_loader_1(fname)
        img2 = self.one_item_loader_2(fname2)

        return img, img2, target

    def __len__(self):
        return len(self.targets)


##############################################################################################
####################################### GET lOADER ###########################################
##############################################################################################
def _make_balanced_sampler(labels):
    class_counts = np.bincount(labels)
    class_weights = 1. / class_counts
    weights = class_weights[labels]
    return WeightedRandomSampler(weights, len(weights))


def get_style_transform(img_size, prob):
    crop = transforms.RandomResizedCrop(
            img_size, scale=[0.8, 1.0], ratio=[0.9, 1.1], antialias=True)

    rand_crop = transforms.Lambda(
            lambda x: crop(x) if random.random() < prob else x)

    style_transform = transforms.Compose([
        rand_crop,
        transforms.Resize([img_size, img_size]),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=0.5, std=0.5),
    ])
    
    return style_transform


def get_gim_transform(img_size, prob):
    crop = transforms.RandomResizedCrop(
            img_size, scale=[0.8, 1.0], ratio=[0.9, 1.1], antialias=True)

    rand_crop = transforms.Lambda(
            lambda x: crop(x) if random.random() < prob else x)

    gim_transform = transforms.Compose([
        rand_crop,
        transforms.Resize([img_size, img_size]),
        transforms.RandomHorizontalFlip(),
        transforms.Normalize(mean=255 / 2, std=255 / 2)
    ])
    
    return gim_transform


def get_loader(root, 
               which='', 
               img_size=256, batch_size=8, prob=0.5, num_workers=4):

    style_transform = get_style_transform(img_size, prob)
    gim_transform = get_gim_transform(img_size, prob)

    if which == 'source_style':
        dataset = StyleDataset(root,
                               transform=style_transform,
                               with_empty_mask=True,
                               channels=1)
    elif which == 'source_gim':
        dataset = GIMDataset(root, 
                             formats=['npy'], 
                             th=0.5, 
                             transform=gim_transform)
    elif which == 'reference_style_style':
        dataset = ReferenceDataset(root_1=root,
                                   root_2=root,
                                   formats_1=['png', 'jpg', 'jpeg', 'JPG'],
                                   formats_2=['png', 'jpg', 'jpeg', 'JPG'],
                                   one_item_loader_1=lambda fname: one_item_loader_1(fname,
                                                                                     transform=style_transform,
                                                                                     with_empty_mask=True,
                                                                                     channels=1),
                                   one_item_loader_2=lambda fname: one_item_loader_1(fname,
                                                                                     transform=style_transform,
                                                                                     with_empty_mask=True,
                                                                                     channels=1))
    else:
        raise NotImplementedError

    sampler = _make_balanced_sampler(dataset.targets)
    return data.DataLoader(dataset=dataset,
                           batch_size=batch_size,
                           sampler=sampler,
                           num_workers=num_workers,
                           pin_memory=True,
                           drop_last=True)


class InputFetcher:
    def __init__(self, loader_style, loader_gim, loader_ref_style_style=None, latent_dim=16, mode=''):
        self.loader_style = loader_style
        self.loader_gim = loader_gim
        self.loader_ref_style_style = loader_ref_style_style

        self.latent_dim = latent_dim
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.mode = mode

    def _fetch_style_inputs(self, which):
        try:
            x, y = next(self.style_iter)
        except (AttributeError, StopIteration):
            self.style_iter = iter(self.loader_style)
            x, y = next(self.style_iter)
        return x, y
    
    def _fetch_gim_inputs(self, which):
        try:
            x, y = next(self.gim_iter)
        except (AttributeError, StopIteration):
            self.gim_iter = iter(self.loader_gim)
            x, y = next(self.gim_iter)
        return x, y

    def _fetch_refs(self):
        try:
            x, x2, y = next(self.iter_ref)
        except (AttributeError, StopIteration):
            self.iter_ref = iter(self.loader_ref_style_style)
            x, x2, y = next(self.iter_ref)
        return x, x2, y

    def __next__(self):
        x_style, y_style = self._fetch_style_inputs(which='style')
        x_gim, y_gim = self._fetch_gim_inputs(which='gim')
        if self.mode == 'train':
            x_ref, x_ref2, y_ref = self._fetch_refs()
            z_trg = torch.randn(x_style.size(0), self.latent_dim)
            z_trg2 = torch.randn(x_style.size(0), self.latent_dim)
            inputs = Munch(x_stl=x_style, y_stl=y_style,
                           x_gim=x_gim, y_gim=y_gim,
                           x_ref=x_ref, x_ref2=x_ref2, y_ref=y_ref,
                           z_trg=z_trg, z_trg2=z_trg2)
        elif self.mode == 'val':
            inputs = Munch(x_stl=x_style, y_stl=y_style,
                           x_gim=x_gim, y_gim=y_gim)
        else:
            raise NotImplementedError

        return Munch({k: v.to(self.device)
                      for k, v in inputs.items()})